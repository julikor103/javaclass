package com.Hillel;

public class HW2_3 {

    //The number 6 is a truly great number. Given two int values, a and b, return true if either one is 6.
    // Or if their sum or difference is 6. Note: the function Math.abs(num) computes the absolute value of a number.
    //    love6(6, 4) → true
    //    love6(4, 5) → false
    //    love6(1, 5) → true

    public boolean love6(int a, int b) {

        if (a == 6 || b == 6 || a + b == 6 || Math.abs(a - b) == 6)
            return true;

        return false;
    }


    //You have a red lottery ticket showing ints a, b, and c, each of which is 0, 1, or 2.
    //If they are all the value 2, the result is 10. Otherwise if they are all the same, the result is 5.
    // Otherwise so long as both b and c are different from a, the result is 1. Otherwise the result is 0.

    // redTicket(2, 2, 2) → 10
    // redTicket(2, 2, 1) → 0
    // redTicket(0, 0, 0) → 5

    public int redTicket(int a, int b, int c) {

        int result1 = 10;
        int result2 = 5;
        int result3 = 1;
        if (a == b && b == c) { //If they are all the value 2, the res is 10.
            if (a == 2) {
                return result1;
            }

            return result2;
        }

        if (a != b && a != c) { //Otherwise if they are all the same - res is 5.
            return result3;
        }

        return 0;
    }

    //You have a green lottery ticket, with ints a, b, and c on it.
    //If the numbers are all different from each other, the result is 0.
    // If all of the numbers are the same, the result is 20.
    // If two of the numbers are the same, the result is 10.


    //greenTicket(1, 2, 3) → 0
    //greenTicket(2, 2, 2) → 20
    //greenTicket(1, 1, 2) → 10

    public int greenTicket(int a, int b, int c) {
        if (a != b && b != c) {

            return 0;
        }

        if (a == b && b == c) {

            return 20;
        }
        return 10;
    }


    //Given 2 ints, a and b, return their sum.
    //However, "teen" values in the range 13..19 inclusive, are extra lucky.
    //So if either value is a teen, just return 19.

    // teenSum(3, 4) → 7
    // teenSum(10, 13) → 19
    // teenSum(13, 2) → 19

    public int teenSum(int a, int b) {
        int sum = a + b;
        if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19)) {
            return 19;
        }
        return sum;
    }
    //You have a blue lottery ticket, with ints a, b, and c on it.
    // This makes three pairs, which we'll call ab, bc, and ac.
    // Consider the sum of the numbers in each pair.
    // If any pair sums to exactly 10, the result is 10.
    // Otherwise if the ab sum is exactly 10 more than either bc or ac sums, the result is 5. Otherwise the result is 0.

    //blueTicket(9, 1, 0) → 10
    //blueTicket(9, 2, 0) → 0
    //blueTicket(6, 1, 4) → 10

    public int blueTicket(int a, int b, int c) {
        int ab = a + b;
        int bc = b + c;
        int ac = a + c;
        if (ab == 10 || ac == 10 || bc == 10) {
            return 10;
        }
        if (ab - bc == 10 || ab - ac == 10) {
            return 5;
        }

        return 0;
    }


    //Given 2 ints, a and b, return their sum.
// However, sums in the range 10..19 inclusive, are forbidden, so in that case just return 20.
//
//
//sortaSum(3, 4) → 7
//sortaSum(9, 4) → 20
//sortaSum(10, 11) → 21

    public int sortaSum(int a, int b) {
        int sum = a + b;

        if (sum >= 10 && sum <= 19) {
            return 20;
        }

        return sum;
    }
}